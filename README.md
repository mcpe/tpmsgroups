# TPMSGroups

Repository containing the group-subgroup structure of the Primitive, Diamond, and Gyroid minimal surfaces in the conventional, cubic unit cell.

The repository contains three tables containing the group theoretical information of the subgroups and three figures outlining the group-subgroup lattice graphs for each of the surfaces.

Additionally, three images of the fundamental region of the surfaces are deposited here along with the coordinates needed to reconstruct them. These are in .h2p-format which can be handled using the Python module found at:

[https://gitlab.com/mcpe/H2Tools](https://gitlab.com/mcpe/H2Tools)

The finer details are outlined in:
```
The intrinsic group–subgroup structures of the Diamond and Gyroid minimal surfaces in their conventional unit cells
Pedersen, Robins, and Hyde (2022)
Acta Crystallogr. A 78(1), 56-58
```

The entries relating to the Primitive minimal surface are reproductions of previous results from:
```
2D hyperbolic groups induce three-periodic Euclidean reticulations
Robins, Ramsden, and Hyde (2004)
Eur. Phys. J. B 39(3), 365-375
```

Similarly, the methods used to obtain these results are documented in this publication.
